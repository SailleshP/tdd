﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
   public class ErrorLogger
    {

        public string LastError { get; set; }
        public event EventHandler<Guid> ErrorLogged;
        public void Log(string error)
        {
            if (string.IsNullOrWhiteSpace(error)) throw new ArgumentNullException();
            LastError = error;
            //write the log to a storage 

            //raise an event
            ErrorLogged?.Invoke(this, Guid.NewGuid());
        }
    }
}
