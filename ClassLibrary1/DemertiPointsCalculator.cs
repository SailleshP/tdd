﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class DemertiPointsCalculator
    {
        private const int SpeedLimit = 65;
        private const int MaxSpeed = 300;
        public int CalculatorDemeritPoint(int speed)
        {

            //number of execution execution path 3
            if (speed < 0 || speed > MaxSpeed) throw new ArgumentOutOfRangeException();
            if (speed <= SpeedLimit) return 0;
            const int kmPerDemeritPoint = 5;
            var demeritPoints = (speed - SpeedLimit) / kmPerDemeritPoint;
            return demeritPoints;
        }

    }
}
