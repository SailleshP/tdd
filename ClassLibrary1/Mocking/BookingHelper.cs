﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestNinja.Mocking;

namespace ClassLibrary1.Mocking
{
    public static class BookingHelperWithoutRefactor
    {
        public static string OverlappingBookingExist(Booking booking)
        {
            if (booking.Status.Equals("Cancelled")) return string.Empty;
            var unitOfWork = new UnitOfWork();
            var bookings = unitOfWork.Query<Booking>().Where(b => b.Id != booking.Id && b.Status != "Cancelled");

            var overLappingBooking = bookings.FirstOrDefault(b => booking.ArrivalDate >= b.ArrivalDate
              && booking.ArrivalDate < b.DepartureDate
              || booking.DepartureDate > b.ArrivalDate
              && booking.DepartureDate <= b.DepartureDate);
            return overLappingBooking == null ? string.Empty : overLappingBooking.Reference;
        }
    }



    public  class BookingHelperRefactored
    {
        public  string OverlappingBookingExist(Booking booking,IBookingRepository repository)
        {
            if (booking.Status.Equals("Cancelled")) return string.Empty;
            var bookings = repository.GetActiveBookings(booking.Id);
            var overLappingBooking =
        bookings.FirstOrDefault(
            b => booking.ArrivalDate < b.DepartureDate &&
            booking.DepartureDate > b.ArrivalDate);

            return overLappingBooking == null ? string.Empty : overLappingBooking.Reference;
        }

       
    }

   


    public class Booking
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public string Reference { get;  set; }
    }
}
