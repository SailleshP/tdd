﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Mocking
{
    public class FileDownloader : IFileDownloader
    {
        //private readonly WebClient _client;
       
        //public FileDownloader(WebClient client)
        //{
        //    _client = client;
        //}
        public void DownloadFile(string url, string path)
        {
             WebClient _client = new WebClient();
            _client.DownloadFile(url,path);

        }
    }
}
