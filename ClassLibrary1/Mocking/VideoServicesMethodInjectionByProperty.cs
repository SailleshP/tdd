﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ClassLibrary1.Mocking
{
    public class VideoServicesMethodInjectionByProperty
    {
        private readonly IFileReader _reader;
        private readonly IVideoRepository _videoService;

        public VideoServicesMethodInjectionByProperty(IFileReader reader, IVideoRepository videoService)
        {
            _reader = reader;
            _videoService = videoService;
        }
        //issues
        public string ReadVideoTitle()
        {
            var str = _reader.Read("video.txt"); //external resource file system
            //move all the code which touches a separate class and isolate it from the rest of the code
            var video = JsonConvert.DeserializeObject<Video>(str);
            if (video == null)
            {
                return "Error while parsing the video";
            }
            return video.Title;
        }

        //[]=>""
        //[{},{},{}] id and join them "1,2,3" video ids
        public string GetUnprocessedVideoAsCsv()
        {
            var videoIds = new List<int>();
            foreach (var video in _videoService.GetUnProcessedVideo())
            {
                videoIds.Add(video.Id);
            }

            return string.Join(",", videoIds);
        }
    }
}

