﻿namespace ClassLibrary1.Mocking
{
    public interface IFileReader
    {
        string Read(string Path);
    }


}