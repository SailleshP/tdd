﻿namespace ClassLibrary1.Mocking
{
    public interface IStorage
    {
        int Store(Order order);
    }
}