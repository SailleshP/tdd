﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Mocking
{
   public class EmployeeController
    {
        private readonly VideoContext _db;

        public EmployeeController(VideoContext db)
        {
            _db = db;
        }

        public ActionResult DeleteEmployee(int Id)
        {
            var employee = _db.Employees.Find(Id);
            _db.Employees.Remove(employee);
            _db.SaveChanges();
            return RedirectToAction("Employees");

        }

        public ActionResult RedirectToAction(string employees)
        {
            return new RedirectResult();
        }

    }
}
