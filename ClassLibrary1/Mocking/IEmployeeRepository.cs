﻿namespace ClassLibrary1.Mocking
{
    public interface IEmployeeStorage
    {
        void DeleteEmployee(int Id);
    }
}