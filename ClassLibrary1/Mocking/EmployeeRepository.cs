﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Mocking
{
    //repostiry cannot have save method
    public class EmployeeStorage : IEmployeeStorage
    {
        private readonly VideoContext _db;

        public EmployeeStorage(VideoContext db)
        {
            _db = db;
        }

        public void DeleteEmployee(int Id)
        {
            var employee = _db.Employees.Find(Id);
            if (employee == null) return;
            _db.Employees.Remove(employee);
            _db.SaveChanges();
        }


    }
}
