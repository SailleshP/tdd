﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Mocking
{
    public class VideoRepository : IVideoRepository
    {

        public IEnumerable<Video> GetUnProcessedVideo()
        {
            using (var context = new VideoContext())
            {

                //external dependency
                var videos = (from video in context.Videos
                              where !video.IsProcessed
                              select video).ToList();
                return videos;
            }

        }
    }
}
