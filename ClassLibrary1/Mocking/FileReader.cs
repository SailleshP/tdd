﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Mocking
{
   public class FileReader : IFileReader
    {
        public string Read(string Path)
        {
            return File.ReadAllText(Path);
        }
    }

    

}
