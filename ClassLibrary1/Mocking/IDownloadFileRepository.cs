﻿namespace ClassLibrary1.Mocking
{
    public interface IFileDownloader
    {
        void DownloadFile(string url, string path);
    }
}