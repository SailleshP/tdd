﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Mocking
{
   public class InstallerHelper
    {
        private readonly IFileDownloader _fileDownloader;
        public InstallerHelper(IFileDownloader fileDownloader)
        {
            _fileDownloader = fileDownloader;
        }

        private string _setupDestinationFile;

        public bool DownloadInstaller(string customerName,string installerName)
        {
            try
            {
                _fileDownloader.DownloadFile(string.Format("http://example/{0}/{1}", customerName, installerName), _setupDestinationFile);
             //touches an external resource
                //client.DownloadFile(string.Format("http://example/{0}/{1}", customerName, installerName), _setupDestinationFile);


                return true;
            }
            catch(WebException)
            {
                return false;
            }
        }


    }
}
