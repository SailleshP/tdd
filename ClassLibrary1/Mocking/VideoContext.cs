﻿using System;
using System.Data.Entity;

namespace ClassLibrary1.Mocking
{
    public class VideoContext:DbContext
    {

        public DbSet<Video> Videos { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}