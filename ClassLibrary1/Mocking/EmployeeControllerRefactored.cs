﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Mocking
{
   public class EmployeeControllerRefactored
    {
        private readonly IEmployeeStorage _employeeRepository;

        public EmployeeControllerRefactored(IEmployeeStorage employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public ActionResult DeleteEmployee(int Id)
        {
            _employeeRepository.DeleteEmployee(Id);
            return RedirectToAction("Employees");

        }

        public ActionResult RedirectToAction(string employees)
        {
            return new RedirectResult();
        }
    }
}
