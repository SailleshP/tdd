﻿using System.Collections.Generic;

namespace ClassLibrary1.Mocking
{
    public interface IVideoRepository
    {
        IEnumerable<Video> GetUnProcessedVideo();
    }
}