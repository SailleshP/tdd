﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Mocking
{
 public   class VideoServicesMethodInjection
    {
        private readonly IVideoRepository _videoService;
        public VideoServicesMethodInjection(IVideoRepository videoService)
        {
            _videoService = videoService;
        }
        //issues
        public string ReadVideoTitle(IFileReader _reader)
        {
            var str = _reader.Read("video.txt"); //external resource file system
            //move all the code which touches a separate class and isolate it from the rest of the code
            var video = JsonConvert.DeserializeObject<Video>(str);
            if (video == null)
            {
                return "Error while parsing the video";
            }
            return video.Title;
        }
        public string GetUnprocessedVideoAsCsv()
        {
            var videoIds = new List<int>();
            //using (var context = new VideoContext())
            //{
            //    var videos = (from video in context.Videos
            //                  where !video.IsProcessed
            //                  select video).ToList();
            foreach (var video in _videoService.GetUnProcessedVideo())
            {
                videoIds.Add(video.Id);
            }
         

                return string.Join(",", videoIds);

            //}
        }
    }
}
