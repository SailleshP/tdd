﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Maths
    {
        public int Sum(int number1,int number2)
        {
            return number1 + number2;
        }

        public int Sum(int [] array)
        {
            var sum = 0;
            foreach (var item in array)
            {
                sum += item;
            }
            return sum;
        }

        public int Max(int a,int b)
        {
            //two conditions two execution paths
            //no of plans is equal to or greater than number of execution plan

            return a > b ? a : b;
        }

        public IEnumerable<int> GetOddNumbers(int Limit)
        {
            for(var i = 0; i <=Limit; i++)
            {
                if (i % 2 != 0) yield return i;
            }
        }

    }
}
