﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
  public  class FizzBuzz
    {
        public static string GetOutput(int number)
        {

            //number of code executions
            //4 so number of test cases 4

            if ((number % 3).Equals(0) && (number % 5).Equals(0))
                return "FizzBuzz";
            if ((number % 3).Equals(0))
                return "Fizz";
            if ((number % 5).Equals(0))
                return "Buzz";
            return number.ToString();
        }


    }
}
