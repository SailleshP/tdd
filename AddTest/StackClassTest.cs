﻿using ClassLibrary1;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AddTest
{
    [TestFixture]
  public  class StackClassTest
    {

        [Test]
        public void Push_WhenArgumentIsNull_ShouldReturnArgumentNullException()
        {
            StackClass<string> stack = new StackClass<string>();
            Assert.That(() =>
            stack.Push(null), Throws.ArgumentNullException);

        }

        [Test]
        public void Push_WhenValidArgument_ShouldContainCountMoreThan1()
        {
            StackClass<string> stack = new StackClass<string>();
            stack.Push("saillesh");
            Assert.That(stack.Count, Is.EqualTo(1));
                
        }
        [Test]
        public void Count_EmptyStack_ReturnZero()
        {
            StackClass<string> stack = new StackClass<string>();
            Assert.That(stack.Count, Is.EqualTo(0));

        }






        [Test]
        public void Pop_StackWithFewObject_ShouldRemovedTheObjectOnTheTop()
        {
            StackClass<string> stack = new StackClass<string>();
            stack.Push("saillesh");
            stack.Push("pawar");
            stack.Pop();
            Assert.That(stack.Count, Is.EqualTo(1));

        }

        [Test]
        public void Pop_StackWithFewObject_ShouldReturnTheRemovedValue()
        {
            //arrange
            StackClass<string> stack = new StackClass<string>();
            stack.Push("saillesh");
            stack.Push("pawar");
            //react
           var result= stack.Pop();
            //assert
            Assert.That(result, Is.EqualTo("pawar").IgnoreCase);

        }


        [Test]
        public void Peek_WhenListIsNotNull_ShouldReturnThePeekedValue()
        {
            StackClass<string> stack = new StackClass<string>();
            stack.Push("saillesh");
            stack.Push("pawar");
            stack.Peek();
            Assert.That("saillesh", Is.EqualTo("saillesh").IgnoreCase);

        }


        [Test]
        public void Peek_WhenListIsNotNull_DoesnotRemoveObjectFromTopOfTheStack()
        {
            StackClass<string> stack = new StackClass<string>();
            stack.Push("saillesh");
            stack.Push("pawar");
            stack.Peek();
            Assert.That(stack.Count, Is.EqualTo(2));

        }

        [Test]
        public void Pop_WhenStackIsEmpty_ShouldReturnInvalidOperationException()
        {
            StackClass<string> stack = new StackClass<string>();
            Assert.That(() =>
            stack.Pop(), Throws.InvalidOperationException);

        }

        [Test]
        public void Peek_WhenListIsEmpty_ShouldReturnInvalidOperationException()
        {
            StackClass<string> stack = new StackClass<string>();
            Assert.That(() =>
            stack.Peek(), Throws.InvalidOperationException);

        }

    }
}
