﻿using ClassLibrary1;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AddTest
{
    [TestFixture]
    public class DemertiPointsCalculatorTest
    {

        [Test]
        [TestCase(-1)]
        [TestCase(301)]
        public void CalculatorDemeritPoint_WhenSpeedIsOutOfRange_ShouldReturnArgumentOutOfRangeException(int speed)
        {
            DemertiPointsCalculator calculator = new DemertiPointsCalculator();
            Assert.That(() =>
                calculator.CalculatorDemeritPoint(speed), Throws.Exception.TypeOf<ArgumentOutOfRangeException>());
        }

       



        [Test]
        [TestCase (0,0)]
        [TestCase(64, 0)]
        [TestCase(65, 0)]
        [TestCase(66, 0)]
        [TestCase(70, 1)]
        [TestCase(75, 2)]
        public void CalculatorDemeritPoint_WhenCalled_ReturnDemertiPoints(int speed,int expectedResult)
        {
            DemertiPointsCalculator calculator = new DemertiPointsCalculator();
            var result = calculator.CalculatorDemeritPoint(speed);
            Assert.That(result, Is.EqualTo(expectedResult));
        }


        [Test]
        public void CalculatorDemeritPoint_WhenSpeedIsGreaterThanSpeedLimit_ShouldReturn4DemeritPoints()
        {
            DemertiPointsCalculator calculator = new DemertiPointsCalculator();
            var result = calculator.CalculatorDemeritPoint(85);
            Assert.That(result, Is.EqualTo(4));
        }

    }
}
