﻿using ClassLibrary1.Mocking;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AddTest.Mocking
{
    [TestFixture]
    public class CustomerControllerTest
    {
        [Test]
        public void DeleteEmployee_WhenCalled_DeleteTheEmployeeFromDB()
        {
            var storage = new Mock<IEmployeeStorage>();
            EmployeeControllerRefactored controller = new EmployeeControllerRefactored(storage.Object);
            controller.DeleteEmployee(1);
            storage.Verify(s => s.DeleteEmployee(1));
        }



    }
}
