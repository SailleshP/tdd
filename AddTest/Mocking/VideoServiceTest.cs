﻿using ClassLibrary1.Mocking;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AddTest.Mocking
{
    [TestFixture]
    class VideoServiceTest
    {
        private Mock<IFileReader> fileReader;
        private Mock<IVideoRepository> repository;
        private VideoServicesMethodInjection _videoService;
        [SetUp]
        public void Setup()
        {

            fileReader = new Mock<IFileReader>();
            repository = new Mock<IVideoRepository>();
            _videoService = new VideoServicesMethodInjection(repository.Object);
        }

        [Test]
        public void ReadVideoTitle_EmptyFile_ShourlReturnErrorMessage()
        {
            
            fileReader.Setup(x => x.Read("video.txt")).Returns("");
            var result= _videoService.ReadVideoTitle(fileReader.Object);
            Assert.That(result, Does.Contain("error").IgnoreCase);
        }

        [Test]
        public void ReadVideoTitleConstructorInjection_EmptyFile_ShourlReturnErrorMessage()
        {

            var service = new VideoServicesMethodInjectionByProperty(new FakeFileReader(), repository.Object);
            var result = service.ReadVideoTitle();
            Assert.That(result, Does.Contain("error").IgnoreCase);
        }

        [Test]
        public void GetUnprocessedVideoAsCsv_AllVideosAreProcessed_ShouldReturnEmptyString()
        {

            repository.Setup(x => x.GetUnProcessedVideo()).Returns(new List<Video>());
            var service = new VideoServicesMethodInjectionByProperty(new FakeFileReader(), repository.Object);
            
            var result = service.GetUnprocessedVideoAsCsv();
            Assert.That(result, Is.EqualTo(""));
        }

        [Test]
        public void GetUnprocessedVideoAsCsv_FewUnProcessedVideos_ShouldReturnStringWithIdOfUnprocessedVideos()
        {

            repository.Setup(x => x.GetUnProcessedVideo()).Returns(new List<Video> {
                new Video{Id=1},
                new Video{Id=2},
                new Video{Id=3}

            });
            var service = new VideoServicesMethodInjectionByProperty(new FakeFileReader(), repository.Object);

            var result = service.GetUnprocessedVideoAsCsv();
            Assert.That(result, Is.EqualTo("1,2,3"));
        }


    }
}
