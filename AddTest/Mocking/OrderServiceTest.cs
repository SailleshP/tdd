﻿using ClassLibrary1.Mocking;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AddTest.Mocking
{
    [TestFixture]
   public class OrderServiceTest
    {



        [Test]
        public void PlaceOrder_WhenCaled_StoreTheOrder()
        {
            var storagage = new Mock<IStorage>();
            var service = new OrderService(storagage.Object);
            var order = new Order();
            service.PlaceOrder(order);
            storagage.Verify(s => s.Store(order));
        }
    }
}
