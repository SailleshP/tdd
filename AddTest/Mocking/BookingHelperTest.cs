﻿using ClassLibrary1.Mocking;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddTest.Mocking
{
    [TestFixture]
   public class BookingHelper_OverLappingBookingTest
    {
        private Booking _existingBooking;
        private  Mock<IBookingRepository> _bookingRepostory;
        

        [SetUp]
        public void Setup()
        {
            _bookingRepostory = new Mock<IBookingRepository>();
            _existingBooking = new Booking()
            {
                Id = 2,
                ArrivalDate = ArrivalOn(2017, 1, 15),
                DepartureDate = DepartureOn(2017, 1, 20),
                Reference = "a"
            };

            _bookingRepostory.Setup(r => r.GetActiveBookings(1)).Returns(new List<Booking>
            {
                _existingBooking
            }.AsQueryable());



        }

        [Test]
        public void BookingStartsAndFinishesBeforeAnExistingBooking_ReturnEmptyString()
        {
            var booking = new BookingHelperRefactored();
           var result= booking.OverlappingBookingExist(new Booking()
            {
                Id = 1,
                Status= "Cancelled",
                ArrivalDate =Before(_existingBooking.ArrivalDate,days:2),
                DepartureDate = After(_existingBooking.ArrivalDate),
            }, _bookingRepostory.Object);

            Assert.That(result, Is.Empty);
        }


        [Test]
        public void BookingStartsAndFinishesInTheMiddleOfAnExistingBooking_ReturnExistingBookingReference()
        {
           

            var booking = new BookingHelperRefactored();
            var result = booking.OverlappingBookingExist(new Booking()
            {
                Id = 1,
                Status = "",
                ArrivalDate = Before(_existingBooking.ArrivalDate),
                DepartureDate = After(_existingBooking.ArrivalDate),
                Reference= "a"
            }, _bookingRepostory.Object);

            Assert.That(result, Is.EqualTo(_existingBooking.Reference));
        }

        [Test]
        public void BookingStartsAndFinishesAfterTheExistingBooking_ReturnExistingBookingReference()
        {
           
            var booking = new BookingHelperRefactored();
            var result = booking.OverlappingBookingExist(new Booking()
            {
                Id = 1,
                Status = "",
                ArrivalDate = Before(_existingBooking.ArrivalDate),
                DepartureDate = After(_existingBooking.DepartureDate), 
            }, _bookingRepostory.Object);

            Assert.That(result, Is.EqualTo(_existingBooking.Reference));
        }

        [Test]
        public void BookingStartsAndFinishesInMiddleOfTheExistingBooking_ReturnExistingBookingReference()
        {

            var booking = new BookingHelperRefactored();
            var result = booking.OverlappingBookingExist(new Booking()
            {
                Id = 1,
                Status = "",
                ArrivalDate = After(_existingBooking.ArrivalDate),
                DepartureDate = Before(_existingBooking.DepartureDate),
            }, _bookingRepostory.Object);

            Assert.That(result, Is.EqualTo(_existingBooking.Reference));
        }


        [Test]
        public void BookingStartsInTheMiddleOfExistingBookingButFinishesAfter_ReturnExistingBookingReference()
        {

            var booking = new BookingHelperRefactored();
            var result = booking.OverlappingBookingExist(new Booking()
            {
                Id = 1,
                Status = "",
                ArrivalDate = After(_existingBooking.ArrivalDate),
                DepartureDate = After(_existingBooking.DepartureDate),
            }, _bookingRepostory.Object);

            Assert.That(result, Is.EqualTo(_existingBooking.Reference));
        }

        [Test]
        public void BookingStartsAndFinishesAfterExisting_ReturnEmptyString()
        {

            var booking = new BookingHelperRefactored();
            var result = booking.OverlappingBookingExist(new Booking()
            {
                Id = 1,
                Status = "",
                ArrivalDate = After(_existingBooking.DepartureDate),
                DepartureDate = After(_existingBooking.DepartureDate,days:2),
            }, _bookingRepostory.Object);

            Assert.That(result, Is.Empty);
        }


        private DateTime Before( DateTime dateTime,int days=1)
        {
            return dateTime.AddDays(-days);
        }

        private DateTime After( DateTime dateTime, int days = 1)
        {
            return dateTime.AddDays(days);
        }

        private DateTime ArrivalOn(int year,int month,int day)
        {
            return new DateTime(year, month, day, 14, 0, 0);
        }
        private DateTime DepartureOn(int year, int month, int day)
        {
            return new DateTime(year, month, day, 10, 0, 0);
        }

    }
}
