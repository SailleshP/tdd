﻿using ClassLibrary1.Mocking;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace AddTest.Mocking
{
    [TestFixture]
   public class InstallerHelperTest
    {
        private Mock<IFileDownloader> fileDownloader;
        private InstallerHelper installerHelper;
        [SetUp]
        public void Setup()
        {
            fileDownloader = new Mock<IFileDownloader>();
            installerHelper = new InstallerHelper(fileDownloader.Object);
        }

        [Test]
        public void DownloadInstaller_DownloadFails_ShouldReturnFalse()
        {
            fileDownloader.Setup(r => r.DownloadFile(It.IsAny<string>(), It.IsAny<string>())).Throws<WebException>();
            var result= installerHelper.DownloadInstaller("customer", "installer");
            Assert.That(result, Is.False);

        }

        [Test]
        public void DownloadInstaller_DownloadComplete_ShouldReturnTrue()
        {
           var result = installerHelper.DownloadInstaller("customer", "installer");
            Assert.That(result, Is.True);

        }

    }
}
