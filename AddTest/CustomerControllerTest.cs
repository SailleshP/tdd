﻿using ClassLibrary1;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AddTest
{
    [TestFixture]
   public class CustomerControllerTest
    {
        [Test]
        public void GetCustomer_IdIsZero_ShouldReturnNotFound()
        {
            CustomerController customer = new CustomerController();
            var result=customer.GetCustomer(0);

            //exactly a not found object
            Assert.That(result, Is.TypeOf<NotFound>());
            //not found object or On of it's derivatives
            Assert.That(result, Is.InstanceOf<NotFound>());
        }

        [Test]
        public void GetCustomer_IdNotZero_ShouldReturnOkay()
        {

        }

    }
}
