﻿using ClassLibrary1;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AddTest
{
    [TestFixture]
   public class HtmlFormatterTest
    {

        [Test]
        public void FormatAsBold_WhenCalled_ShouldEncloseStringWithStrongElement()
        {
            var formatter = new HtmlFormatter();
            var result=formatter.FormatAsBold("saillesh");
            //specific assertion
            Assert.That(result,Is.EqualTo("<strong>saillesh</strong>").IgnoreCase);
            //more general
            Assert.That(result, Does.StartWith("<strong>").IgnoreCase);

            Assert.That(result, Does.EndWith("</strong>").IgnoreCase);
            Assert.That(result, Does.Contain("saillesh"));
        }
    }
}
