using ClassLibrary1;
using NUnit.Framework;
using System.Linq;

namespace AddTest
{


//    A method annotated with[TestInitialize] is run before each test.Likewise [TestCleanup] is after each test.


//[ClassInitialize] and [ClassCleanup] are run before and after the 'suite' of tests inside the TestClass.


  // [TestClass]
    public class AddTest
    {
        private Maths _math;

        //[TestInitialize] //setup
        [SetUp]
        public void SetUp()
        {
            _math = new Maths();
        }

        //[TestMethod]
        [Test]
        public void AddArray_Pass2Values_Returns3()
        { 
            var result = _math.Sum(new int[] {1,2 });
            Assert.AreEqual(result, 3);
        }


        [Test]
        public void AddTwoNumber_Pass2Values_Returns5()
        {
            
            var result= _math.Sum(2, 3);
            Assert.AreEqual(result, 5);
        }

        [Test]
        public void AddTwoNumber_Pass2NegativeValues_Returns5()
        {
            Maths math = new Maths();
            var result = math.Sum(-2, -3);
            Assert.AreEqual(result, -5);
        }

        [Test]
        public void AddTwoNumber_Pass2Decimal_notReturns5()
        {
           var result = _math.Sum(1,2);
            Assert.AreNotEqual(result, -5);
        }


        [Test]
        public void Max_FirstArgumentIsGreater_ReturnFirstArgument()
        {
            var result= _math.Max(10, 2);
            Assert.AreEqual(result, 10);
        }

        [Test]
        public void Max_SecondArgumentIsGreater_ReturnSecondArgument()
        {
            var result = _math.Max(1, 22);
            Assert.AreEqual(result, 22);
        }

        [Test]
        public void Max_ArgumentEqual_ReturnSameArgument()
        {
            var result = _math.Max(22, 22);
            Assert.AreEqual(result, 22);
        }


        //not to general not to specific
        [Test]
        public void GetOddNumber_LimitGreaterThanZero_ReturnOddNumbersUpToLimit()
        {
           var items= _math.GetOddNumbers(5);
            Assert.That(items, Is.Not.Empty);
            Assert.That(items.Count, Is.EqualTo(3));
            Assert.That(items, Does.Contain(1));

            Assert.That(items, Does.Contain(3));

            Assert.That(items, Does.Contain(5));
            //another way single line
            Assert.That(items, Is.EquivalentTo(new int[] { 1, 3, 5 }));

            Assert.That(items, Is.Ordered);
            Assert.That(items, Is.Unique);


        }
    }
}
