﻿using ClassLibrary1;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AddTest
{
    [TestFixture]
    class ErrorLoggerTest
    {
        [Test]
        public void Log_WhenCalled_SetTheLastErrorProperty()
        {
            ErrorLogger logger = new ErrorLogger();
            logger.Log("a");
            Assert.That(logger.LastError, Is.EqualTo("a"));

        }


        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void Log_InvalidError_ShoudlThrowArgumentNullException(string error)
        {
            ErrorLogger logger = new ErrorLogger();
            Assert.That(() =>
                logger.Log(error),Throws.ArgumentNullException );

            
        }

        [Test]
        public void Log_ValidError_RaiseErrorLoggedEvent()
        {
            ErrorLogger logger = new ErrorLogger();
            var id = Guid.Empty;
            logger.ErrorLogged += (sender, args) =>
            {
                id = args;
            };
            logger.Log("a");
            Assert.That(id, Is.Not.EqualTo(Guid.Empty));

        }
    }
}
