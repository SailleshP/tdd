﻿using ClassLibrary1;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AddTest
{
    [TestFixture]
   public class FizBuzzTest
    {

        [Test]
        public void Getoutput_WhenNumberDivisibleBy3Only_ShouldReturnFizzOnly()
        {

            var result = FizzBuzz.GetOutput(3);
            Assert.That(result, Is.EqualTo("Fizz").IgnoreCase);
        }

        [Test]
        public void Getoutput_WhenNumberDivisibleBy5Only_ShouldReturnBuzzOnly()
        {

            var result = FizzBuzz.GetOutput(5);
            Assert.That(result, Is.EqualTo("Buzz").IgnoreCase);
        }

        [Test]
        public void Getoutput_WhenNumberDivisibleByBoth3And5_ShouldReturnFizBuzz()
        {

            var result = FizzBuzz.GetOutput(15);
            Assert.That(result, Is.EqualTo("FizzBuzz").IgnoreCase);
        }

        [Test]
        public void Getoutput_WhenNumberNotDivisibleByBoth3And5_ShouldReturnPassedNumber()
        {

            var result = FizzBuzz.GetOutput(4);
            Assert.That(result, Is.EqualTo("4"));
        }

    }
}
